<?php

namespace Orchestra\Facade;

use Orchestra\Exceptions\InvalidFacadeConcreteException;

abstract class Facade
{
   public static abstract function concrete(): string;

   public static function __callStatic($method, $args)
   {
      $concrete = static::concrete();

      $instance = new $concrete;

      if (empty($instance)) {
         throw new InvalidFacadeConcreteException('Facade Concrete not defined.');
      }

      return $instance->$method(...$args);
   }
}
