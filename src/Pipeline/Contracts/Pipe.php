<?php

namespace Orchestra\Pipeline\Contracts;

interface Pipe
{
   public function handle(&$data);
}
