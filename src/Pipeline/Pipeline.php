<?php

namespace Orchestra\Pipeline;

use Orchestra\Exceptions\InvalidPipeException;

class Pipeline
{
   private $head;
   private $tail;
   private $data;

   private function __construct($data)
   {
      $this->data = $data;
   }

   public static function send($data)
   {
      return new Pipeline($data);
   }

   public function through(array $pipes)
   {
      array_walk($pipes, function ($pipe) {

         $this->insertPipe($pipe);
      });

      return $this;
   }

   public function run()
   {
      $this->head->handle($this->data);

      return $this;
   }

   public function getReturn()
   {
      return $this->data;
   }

   private function insertPipe($pipe)
   {
      $pipe = $this->instancePipe($pipe);

      if (empty($this->head)) {
         $this->head = $pipe;

         $this->tail = $pipe;

         return;
      }

      $pipe->setPrevious($this->tail);

      $this->tail->setNext($pipe);

      $this->tail = $pipe;
   }

   private function instancePipe($pipe)
   {
      if ($pipe instanceof Pipe) {
         return $pipe;
      }

      if (!is_string($pipe) || !class_exists($pipe)) {
         throw new InvalidPipeException("$pipe is an invalid Pipe");
      }

      return new $pipe();
   }
}
