<?php

namespace Orchestra\Pipeline\Traits;

use Orchestra\Pipeline\Contracts\Pipe as ContractsPipe;

trait Pipe
{
   protected $next;
   protected $previous;

   public function next(&$data)
   {
      if (empty($this->next)) {
         return null;
      }

      return $this->next->handle($data);
   }

   public function setNext(ContractsPipe $next)
   {
      $this->next = $next;
   }

   public function setPrevious(ContractsPipe $previous)
   {
      $this->previous = $previous;
   }
}
