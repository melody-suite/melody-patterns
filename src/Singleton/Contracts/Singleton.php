<?php

namespace Orchestra\Singleton\Contracts;

interface Singleton
{
   public static function getInstance();
}
