<?php

namespace Orchestra\Singleton\Traits;

trait Singleton
{
   protected static $instance;

   public static function getInstance()
   {
      if (empty(static::$instance)) {
         static::$instance = new static();
      }

      return static::$instance;
   }
}
